import javalang
import pprint

import argparse
import os

from javalang import javadoc
from lxml import etree

parser = argparse.ArgumentParser(description='Generates a desc.xml file for the CSV Translator UI')
parser.add_argument('--file', type=str, help='The file you want to be parsed to generate a desc.xml.', required=True)
parser.add_argument('--group', type=str, help='The group you want these functions associated with.', required=True)

args = parser.parse_args()

FILE = args.file

with open(FILE, 'r') as javafile:
    code = javafile.read()

tree = javalang.parse.parse(code)

group = args.group

# Creates a root element named templates
root = etree.Element('templates')
# Iterates over all method declarations
for path, node in tree.filter(javalang.tree.MethodDeclaration):
    if(not node.name.isupper()):
        continue

    params = node.parameters
    docs = javadoc.parse(node.documentation)

    # Creates a new entry in the templates named template
    # with a name and group field.
    template = etree.SubElement(root, 'template', {
        'name'  : node.name.upper(),
        'group' : group
    })

    # Gets the description from the in-code documentation
    desc      = etree.SubElement(template, 'description')
    desc.text = docs.description

    inputs = etree.SubElement(template, 'inputs')

    # Checks if the function takes a variadic argument
    # If so, it just creates it with two inputs.
    if len(params) == 1 and params[0].varargs:
        var1 = etree.SubElement(inputs, 'variable', {
            'name' : 'String A'
        })

        desc = etree.SubElement(var1, 'description')
        desc.text = "The first variable."

        key = etree.SubElement(var1, 'key')
        key.text = "A"

        typ = etree.SubElement(var1, 'type')
        typ.text = "STRING"

        var2 = etree.SubElement(inputs, 'variable', {
            'name' : 'String B'
        })

        desc = etree.SubElement(var2, 'description')
        desc.text = "The second variable."

        key = etree.SubElement(var2, 'key')
        key.text = "B"

        typ = etree.SubElement(var2, 'type')
        typ.text = "STRING"
    else:
        for param, doc in zip(params, docs.params):
            var = etree.SubElement(inputs, 'variable', {
                'name' : param.name.title()
            })
            
            desc = etree.SubElement(var, 'description')
            desc.text = doc[1]

            key = etree.SubElement(var, 'key')
            key.text = param.name.upper()

            typ = etree.SubElement(var, 'type')
            typ.text = param.type.name.upper()
    
    output = etree.SubElement(template, 'output', {
        'name': 'Output',
    })

    out_desc = etree.SubElement(output, 'description')
    out_type = etree.SubElement(output, 'type')
    if node.return_type is None:
        out_type.text = "EXPRESSION"
    else:
        out_type.text = node.return_type.name.upper()


    if docs.return_doc is None:
        out_desc.text = docs.description
    else:
        out_desc.text = docs.return_doc

    # Generates the code string
    code = etree.SubElement(template, 'code')
    code_str = node.name + "("
    if len(params) == 1 and params[0].varargs:
        code_str = code_str + "$A, $B)"
    else:
        first = True
        for param in params:
            if not first:
                code_str = code_str + ','
            else:
                first = False
            code_str = code_str + '$' + param.name.upper()
        code_str = code_str + ')'

    code.text = code_str

print(etree.tostring(root, pretty_print=True).decode('utf-8'))
