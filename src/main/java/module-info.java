module unanet.translator {
    requires javafx.controls;
    requires javafx.fxml;
    requires simple.xml;

    exports unanet.translator;
    exports unanet.translator.beans;
}
