package unanet.translator;

import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.Node;
import java.net.URL;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.fxml.FXML;
import javafx.util.Pair;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import java.io.*;

import java.util.*;
import unanet.translator.beans.*;

public class IfDialogController implements Initializable {
    @FXML
    public Spinner<Integer> spinner;

    @FXML
    public CheckBox has_else;

    @FXML
    public Button done;

    public boolean saved = false;

    @Override
    public void initialize(URL url, ResourceBundle res) {
        spinner.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 1));

        done.setOnAction(e -> {
            saved = true;
            ((Node)e.getSource()).getScene().getWindow().hide();
        });
    }

    public IfDescriptor getReturn() {
        if(!saved) {
            return null;
        }

        return new IfDescriptor((int)spinner.getValue(), has_else.isSelected());
    }
}
