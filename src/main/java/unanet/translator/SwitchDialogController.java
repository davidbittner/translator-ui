package unanet.translator;

import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.Node;
import java.net.URL;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.fxml.FXML;
import javafx.util.Pair;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.collections.ObservableSet;
import javafx.collections.FXCollections;
import java.util.stream.Collectors;
import java.io.*;

import java.util.*;
import unanet.translator.beans.*;

public class SwitchDialogController implements Initializable {
    @FXML
    public Button done;
    @FXML
    public Button cancel;

    @FXML
    public ListView<String> list;

    @FXML
    public TextField insert_string;
    @FXML
    public Button insert_button;

    @FXML
    public CheckBox def;

    public boolean saved = false;

    ObservableSet<String> switch_list;

    @Override
    public void initialize(URL url, ResourceBundle res) {
        done.setOnAction(e -> {
            saved = true;
            ((Node)e.getSource()).getScene().getWindow().hide();
        });

        cancel.setOnAction(e -> {
            saved = false;
            ((Node)e.getSource()).getScene().getWindow().hide();
        });

        switch_list = FXCollections.observableSet();
    }

    @FXML
    public void addString() {
        switch_list.add(insert_string.getText());
        insert_string.setText("");
        list.setItems(FXCollections.observableArrayList(switch_list));
    }

    @FXML
    public void deleteSelected() {
        String selected = list.getSelectionModel().getSelectedItem();
        switch_list.remove(selected);
        list.setItems(FXCollections.observableArrayList(switch_list));
    }

    public SwitchDescriptor getReturn() {
        return new SwitchDescriptor(list.getItems(), def.isSelected());
    }
}
