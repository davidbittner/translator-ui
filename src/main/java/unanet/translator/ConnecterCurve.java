package unanet.translator;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.*;
import javafx.scene.canvas.*;
import javafx.scene.Parent;
import javafx.scene.shape.*;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.ClipboardContent; 
import javafx.scene.input.Dragboard; 
import javafx.scene.input.TransferMode; 
import javafx.scene.input.DragEvent; 
import javafx.scene.transform.Translate;
import javafx.scene.effect.DropShadow; 

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import javafx.beans.Observable;

import java.util.UUID;

class ConnecterCurve extends CubicCurve {
    NodeConnecter a;
    NodeConnecter b;

    public void update() {
        double apx = a.getParent().getLayoutX();
        double apy = a.getParent().getLayoutY();

        double bpx = b.getParent().getLayoutX();
        double bpy = b.getParent().getLayoutY();

        setStartX(a.getLayoutX() + apx);
        setStartY(a.getLayoutY() + apy);

        setControlX1((apx + a.getLayoutX() + bpx + b.getLayoutX())/2.0);
        setControlX2((apx + a.getLayoutX() + bpx + b.getLayoutX())/2.0);

        setControlY1(a.getLayoutY() + apy);
        setControlY2(b.getLayoutY() + bpy);

        setEndX(b.getLayoutX() + bpx);
        setEndY(b.getLayoutY() + bpy);
    }

    ConnecterCurve(NodeConnecter a, NodeConnecter b) {
        this.a = a;
        this.b = b;

        setFill(null);
        setStroke(Colors.ACCENT_B);
        setStrokeWidth(2.0);
        setMouseTransparent(true);

        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

        setEffect(dropShadow);

        update();
        a.getParent().layoutXProperty().addListener(e -> update());
        a.getParent().layoutYProperty().addListener(e -> update());
        b.getParent().layoutXProperty().addListener(e -> update());
        b.getParent().layoutYProperty().addListener(e -> update());
    }
}
