package unanet.translator;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.util.Pair;
import java.io.Serializable;
import java.util.Objects;

import unanet.translator.beans.*;

class CommentState implements Serializable {
    String comment_text;
    double x, y;

    public CommentState(VisualCommentNode comment) {
        this.x = comment.getLayoutX();
        this.y = comment.getLayoutY();

        this.comment_text = comment.text.getText();
    }
}

class ConnecterState implements Serializable {
    public ConnecterState(String id, String vari_key) {
        this.id = id;
        this.vari_key = vari_key;
    }

    String id;
    String vari_key;
}

class NodeState implements Serializable {
    public NodeState(TranslatorNode node) {
        x = node.getLayoutX();
        y = node.getLayoutY();

        desc = node.desc;

        connectors = new ArrayList<>();
        for(NodeConnecter conn : node.conns) {
            connectors.add(new ConnecterState(conn.id, conn.info.subst_name));
        }
    }

    double x, y;
    NodeDescriptor desc;

    List<ConnecterState> connectors;
}

class EditorState implements Serializable {
    static final long serialVersionUID = -1404675131860317514l;

    String name;
    List<Pair<String, String>> connections;
    List<NodeState> nodes;
    List<CommentState> comments;

    public EditorState(NodeEditor editor, String name) {
        this.nodes       = new ArrayList<>();
        this.connections = new ArrayList<>();
        this.comments    = new ArrayList<>();
        this.name = name;

        for(TranslatorNode node : editor.nodes) {
            this.nodes.add(new NodeState(node));
        }

        for(VisualCommentNode node : editor.comments) {
            this.comments.add(new CommentState(node));
        }

        for(Map.Entry<String, String> entry : editor.connections.entrySet()) {
            connections.add(new Pair<String, String>(entry.getKey(), entry.getValue()));
        }
    }

    @Override
    public int hashCode() {
        return (connections.hashCode() ^ nodes.hashCode());
    }
}

public class SaveState implements Serializable {
    static final long serialVersionUID = -4220580037530973371l;

    public SaveState(List<Pair<String, NodeEditor>> editors, Map<String, NodeDescriptor> dict, HeaderBlock block) {
        this.editors = new ArrayList<>();
        this.header_block = block;

        for(Pair<String, NodeEditor> editor : editors) {
            this.editors.add(new EditorState(editor.getValue(), editor.getKey()));
        }
    }

    HeaderBlock header_block;
    List<EditorState> editors;

    @Override
    public int hashCode() {
        return (header_block.hashCode() ^ editors.hashCode());
    }
}
