package unanet.translator;

import javafx.scene.paint.*;

public class Colors {
    static final Color FOREGROUND = Color.web("#6ab568");
    static final Color BACKGROUND = Color.web("#01b06a");

    static final Color COMMENT_BACKGROUND  =
        Color.web("#f5f5f5");
    static final Color COMMENT_HBACKGROUND = 
        Color.web("#f0f0f0");
    static final Color COMMENT_PBACKGROUND = 
        Color.web("#cfcfcf");
    static final Color COMMENT_FOREGROUND  = 
        Color.web("#000000");

    static final Color NODE_HOVER = Color.web("#72bf70");
    static final Color NODE_DRAG  = Color.web("#7acc78");

    static final Color ACCENT_B   = Color.web("#6692cd");
    static final Color ACCENT_C   = Color.web("#76a7e8");

    static final Color EXPRESSION_TYPE = ACCENT_C;
    static final Color STRING_TYPE     = Color.web("#85e876");
    static final Color INT_TYPE        = Color.web("#d876e8");
    static final Color BOOLEAN_TYPE    = Color.web("#e87678");
}
