package unanet.translator;

import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.Node;
import java.net.URL;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.fxml.FXML;
import javafx.util.Pair;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import java.io.*;

import java.util.*;
import unanet.translator.beans.*;

public class LoadDialogController implements Initializable {
    @FXML
    public TextField file_path;
    @FXML
    public Button file_opener;
    @FXML
    public TextField file_id;

    @FXML
    public Spinner<Integer> line_number;
    @FXML
    public CheckBox enabled;
    @FXML
    public CheckBox relative;

    @FXML
    public Button done;

    public boolean saved = false;

    @Override
    public void initialize(URL url, ResourceBundle res) {
        line_number.setValueFactory(new IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 1));
        
        done.setOnAction(e -> {
            saved = true;
            ((Node)e.getSource()).getScene().getWindow().hide();
        });
    }

    public LoadFile getReturn() {
        if(!saved) {
            return null;
        }
        LoadFile ret = new LoadFile();
        ret.setFilePath(file_path.getText());
        ret.setFileId(file_id.getText());

        if(enabled.isSelected()) {
            ret.use_number = true;
            ret.is_relative = relative.isSelected();
            ret.line_no = (int)line_number.getValue();
        }

        return ret;
    }

    @FXML
    public void selectFile() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Pick a CSV File");
        chooser.getExtensionFilters().addAll(
            new ExtensionFilter("CSV File", "*.csv"),
            new ExtensionFilter("TXT File", "*.txt"),
            new ExtensionFilter("Any File", "*")
        );

        File picked = chooser.showOpenDialog(file_path.getScene().getWindow());
        if(picked != null) {
            file_path.setText(picked.toString());
        }
    }
}
