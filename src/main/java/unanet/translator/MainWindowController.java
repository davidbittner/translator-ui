package unanet.translator;

import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXML;
import javafx.scene.control.TextInputDialog;
import javafx.scene.Scene;
import java.io.*;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.Accordion;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.Alert;
import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;
import java.util.*;
import java.net.URL;
import javafx.fxml.Initializable;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.Serializer;
import unanet.translator.beans.*;

public class MainWindowController implements Initializable {
    @FXML
    public Menu menu_file;

    @FXML
    public TabPane tab_pane;

    @FXML
    public Accordion selector_accordion;

    @FXML
    public VBox main_vbox;

    Map<String, NodeDescriptor> node_dictionary;

    HeaderBlock header_block;

    File saved_to;
    int saved_hash;

    @Override
    public void initialize(URL url, ResourceBundle res) {
        node_dictionary = new TreeMap<>();
        File file = new File("desc.xml");
        if(file.exists()) {
            importTemplates(file);
        }
    }

    @FXML
    public void keyPress(KeyEvent ev) {
        if(ev.getCode() == KeyCode.Z && ev.isControlDown()) {
            Tab current = tab_pane.getSelectionModel().getSelectedItem();
            if(current != null) {
                NodeEditor editor = (NodeEditor)current.getContent();
                editor.undo();
            }
        }
    }

    @FXML
    public void importHeaders() {
        if(pendingChanges()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Unsaved Changes");
            alert.setHeaderText("Unsaved Changes");
            alert.setContentText("Are you sure you would like to continue without saving?");

            Optional<ButtonType> button = alert.showAndWait();
            if(button.get() != ButtonType.OK) {
               return;
            }
        }

        Parent root;
        try{
            Stage dialog = new Stage();
            dialog.initOwner(selector_accordion.getScene().getWindow());
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setTitle("Import Headers");
            dialog.setResizable(true);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("header_import.fxml"));
            root = loader.load();
            ImportHeaderController cont = loader.getController();

            dialog.setScene(new Scene(root));

            dialog.showAndWait();
            cont.done();

            header_block = cont.getBlock();
            populateTabs(header_block.headers);
        }catch(IOException ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Opening Dialog");
            alert.setContentText("Failed to open the dialog due to an IOException.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    @FXML
    public void newTemplate() {
        if(pendingChanges()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Unsaved Changes");
            alert.setHeaderText("Unsaved Changes");
            alert.setContentText("Are you sure you would like to continue without saving?");

            Optional<ButtonType> button = alert.showAndWait();
            if(button.get() != ButtonType.OK) {
               return;
            }
        }

        Parent root;
        try{
            Stage dialog = new Stage();
            dialog.initOwner(selector_accordion.getScene().getWindow());
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setTitle("New Template");
            dialog.setResizable(true);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("new_dialog.fxml"));
            root = loader.load();
            NewTemplateController cont = loader.getController();

            dialog.setScene(new Scene(root));
            dialog.showAndWait();

            cont.done();
            header_block = cont.getBlock();

            populateTabs(header_block.headers);
        }catch(IOException ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Opening Dialog");
            alert.setContentText("Failed to open the dialog due to an IOException.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    public void populateTabs(String headerList) {
        String []headers = headerList.split(",", -1);
        tab_pane.getTabs().clear();

        if(header_block.has_logic_only) {
            NodeEditor logic_editor = new NodeEditor(node_dictionary, true);
            Tab logic_tab = new Tab("Logic Editor", logic_editor);
            logic_tab.setStyle(
                "-fx-font-weight: bold"
            );

            tab_pane.getTabs().add(logic_tab);
        }

        Tab tabs[] = new Tab[headers.length];
        for(int i = 0; i < headers.length; i++) {
            tabs[i] = new Tab();
            NodeEditor editor = new NodeEditor(node_dictionary, false);

            tabs[i].setText(headers[i]);
            tabs[i].setContent(editor);
            editor.createNewComment("Hello, world!");
        }

        tab_pane.getTabs().addAll(tabs);
    }

    public void importMoreTemplates() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Pick an XML File");
        chooser.getExtensionFilters().addAll(
            new ExtensionFilter("XML File", "*.xml"),
            new ExtensionFilter("All Files", "*")
        );

        List<File> picked = chooser.showOpenMultipleDialog(tab_pane.getScene().getWindow());

        if(picked != null) {
            for(File file : picked) {
                importTemplates(file);
            }
        }
    }

    public void importTemplates(File dictionary_reader) {
        try{
            Persister serializer = new Persister();

            NodeDescriptorList node_list;
            node_list = serializer.read(NodeDescriptorList.class, dictionary_reader);
            for(NodeDescriptor desc : node_list.nodes) {
                node_dictionary.put(desc.group + desc.name, desc);
            }
        }catch(Exception ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Importing Templates");
            alert.setContentText(ex.toString());

            alert.showAndWait();

            ex.printStackTrace();
        }finally {
            populateAccordion();
        }
    }

    void populateAccordion() {
        selector_accordion.getPanes().clear();

        HashMap<String, TitledPane> groups = new HashMap<>();
        for(Map.Entry<String, NodeDescriptor> entry : node_dictionary.entrySet()) {
            NodeDescriptor desc = entry.getValue();
            if(groups.get(desc.group) == null) {
                TitledPane add = new TitledPane(desc.group, new VBox());
                ((VBox)add.getContent())
                    .getChildren().add(new NodeDescriptorBox(desc));

                groups.put(desc.group, add);

                selector_accordion.getPanes().add(add);
            }else{
                TitledPane pane = groups.get(desc.group);
                ((VBox)pane.getContent())
                    .getChildren().add(new NodeDescriptorBox(desc));
            }
        }
    }

    @FXML
    public void exportTemplate() {
        if(header_block == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Saving Template");
            alert.setContentText("A new template file must be created or imported to populate the header tabs.");

            alert.showAndWait();

            return;
        }

        String blocks[] = new String[tab_pane.getTabs().size()];

        int block = 0;
        for(Tab pane : tab_pane.getTabs()) {
            NodeEditor ed = (NodeEditor)pane.getContent();
            blocks[block] = ed.evaluate();

            block++;
        }

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose a Save Location");
        chooser.getExtensionFilters().addAll(
            new ExtensionFilter("TXT File", "*.txt"),
            new ExtensionFilter("All Files", "*")
        );

        File picked = chooser.showSaveDialog(tab_pane.getScene().getWindow());
        if(picked != null) {
            try{
                FileWriter writer = new FileWriter(picked);
                writer.write(header_block.toString());
                writer.write("END\n");

                for(String block_str : blocks) {
                    writer.write(block_str+"\n");
                    writer.write("END\n");
                }

                writer.close();
            }catch(IOException ex) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error Saving Template");
                alert.setContentText(ex.toString());

                ex.printStackTrace();
                alert.showAndWait();
            }
        }
    }

    private SaveState generateSaveState() {
        List<Pair<String, NodeEditor>> eds = new ArrayList<>();
        for(Tab pane : tab_pane.getTabs()) {
            NodeEditor ed = (NodeEditor)pane.getContent();
            String name = pane.getText();
            eds.add(new Pair<String, NodeEditor>(name, ed));
        }

        return new SaveState(eds, node_dictionary, this.header_block);
    }

    public boolean pendingChanges() {
        if(header_block == null) {
            return false;
        }

        SaveState current_state = generateSaveState();
        return current_state.hashCode() != saved_hash;
    }

    @FXML
    public void saveAs() {
        saved_to = null;
        saved_hash = 0;
        saveState();
    }

    @FXML
    public void saveState() {
        if(header_block == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Saving State");
            alert.setContentText("No project is active, there is nothing to save.");

            alert.showAndWait();

            return;
        }

        File picked;
        if(saved_to == null) {
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Choose a Save Location");
            chooser.getExtensionFilters().addAll(
                new ExtensionFilter("Translator State File (tsf)", "*.tsf"),
                new ExtensionFilter("All Files", "*")
            );

            picked = chooser.showSaveDialog(tab_pane.getScene().getWindow());
            if(picked != null) {
                saved_to = picked;
            }
        }else{
            picked = saved_to;
        }

        if(picked != null) {
            main_vbox.setDisable(true);
            Platform.runLater(() -> {
                SaveState state = generateSaveState();

                try{
                    FileOutputStream writer = new FileOutputStream(picked);
                    ObjectOutputStream obj_writer = new ObjectOutputStream(writer);

                    obj_writer.writeObject(state);
                    obj_writer.close();

                    saved_hash = state.hashCode();
                }catch(IOException ex) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error Saving State");
                    alert.setContentText(ex.toString());

                    ex.printStackTrace();
                    alert.showAndWait();
                }finally{
                    main_vbox.setDisable(false);
                }
            });
        }
    }

    @FXML
    public void loadState() {
        if(pendingChanges()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Unsaved Changes");
            alert.setHeaderText("Unsaved Changes");
            alert.setContentText("Are you sure you would like to continue without saving?");

            Optional<ButtonType> button = alert.showAndWait();
            if(button.get() != ButtonType.OK) {
               return;
            }
        }

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Pick a TSF File");
        chooser.getExtensionFilters().addAll(
            new ExtensionFilter("TSF File", "*.tsf"),
            new ExtensionFilter("All Files", "*")
        );

        File picked = chooser.showOpenDialog(tab_pane.getScene().getWindow());
        if(picked != null) {
            main_vbox.setDisable(true);
            Platform.runLater(() -> {
                try{
                    FileInputStream reader = new FileInputStream(picked);
                    ObjectInputStream obj_reader = new ObjectInputStream(reader);

                    SaveState state = (SaveState)obj_reader.readObject();
                    this.header_block = state.header_block;

                    tab_pane.getTabs().clear();
                    boolean first = header_block.has_logic_only;

                    for(EditorState editor_st : state.editors) {
                        NodeEditor editor = new NodeEditor(node_dictionary, first);

                        editor.populate(editor_st);

                        Tab tab = new Tab(editor_st.name, editor);
                        if(first) {
                            tab.setStyle(
                                "-fx-font-weight: bold"
                            );
                        }
                        tab_pane.getTabs().add(tab);
                        first = false;
                    }
                    saved_hash = state.hashCode();
                    saved_to = picked;
                }catch(InvalidClassException ex) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error Loading State");
                    alert.setContentText("This save is no longer compatible with this version.");

                    ex.printStackTrace();
                    alert.showAndWait();
                }catch(Exception ex) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error Loading State");
                    alert.setContentText(ex.toString());

                    ex.printStackTrace();
                    alert.showAndWait();
                }finally{
                    main_vbox.setDisable(false);
                }
            });

        }
    }

    private void insertToSelected(NodeDescriptor desc) {
        Tab selected = tab_pane.getSelectionModel().getSelectedItem();
        if(selected == null){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Inserting Node");
            alert.setContentText("You currently do not have a selected tab to add a node to.");

            alert.showAndWait();
            return;
        }

        NodeEditor ed = (NodeEditor)selected.getContent();
        ed.createNewNode(desc);
    }

    @FXML
    public void insertConstant() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Constant Value");
        dialog.setHeaderText("Enter a Constant Value");
        dialog.setContentText("Constant Value:");

        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            ConstantDescriptor desc = new ConstantDescriptor(result.get());
            insertToSelected(desc);
        }
    }

    @FXML
    public void insertVisualComment() {
        Optional<String> text = VisualCommentNode.displayTextArea("");
        if(text.isPresent()) {
            Tab selected = tab_pane.getSelectionModel().getSelectedItem();
            if(selected == null){
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error Inserting Node");
                alert.setContentText("You currently do not have a selected tab to add a node to.");

                alert.showAndWait();
                return;
            }

            NodeEditor ed = (NodeEditor)selected.getContent();
            ed.createNewComment(text.get());
        }
    }

    @FXML public void insertIncodeComment() {
        Optional<String> comment_text = VisualCommentNode.displayTextArea("");
        comment_text.ifPresent((t) -> {
            insertToSelected(new CommentDescriptor(t));
        });
    }

    @FXML
    public void insertIfStatement() {
        Parent root;
        try{
            Stage dialog = new Stage();
            dialog.initOwner(selector_accordion.getScene().getWindow());
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setTitle("If-Statement");
            dialog.setResizable(true);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("ifdialog.fxml"));
            root = loader.load();

            dialog.setScene(new Scene(root));

            dialog.showAndWait();
            IfDialogController cont = loader.getController();

            NodeDescriptor desc = cont.getReturn();
            insertToSelected(desc);
        }catch(IOException ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Opening Dialog");
            alert.setContentText("Failed to open the dialog due to an IOException.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    @FXML
    public void insertSwitchStatement() {
        Parent root;
        try{
            Stage dialog = new Stage();
            dialog.initOwner(selector_accordion.getScene().getWindow());
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setTitle("If-Statement");
            dialog.setResizable(true);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("switch_dialog.fxml"));
            root = loader.load();

            dialog.setScene(new Scene(root));

            dialog.showAndWait();
            SwitchDialogController cont = loader.getController();

            NodeDescriptor desc = cont.getReturn();
            insertToSelected(desc);
        }catch(IOException ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Opening Dialog");
            alert.setContentText("Failed to open the dialog due to an IOException.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    @FXML
    public void insertConnectorBlock() {
        Dialog<Integer> dialog = new Dialog<>();
        dialog.setTitle("Connector Block Dialog");
        dialog.setHeaderText("How many connectors do you need?");

        ButtonType done = new ButtonType("Done", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(done);

        Spinner<Integer> connectors = new Spinner<>();
        connectors.setValueFactory(new IntegerSpinnerValueFactory(2, Integer.MAX_VALUE, 2, 1));

        dialog.getDialogPane().setContent(connectors);

        dialog.setResultConverter(e -> {
            return connectors.getValue();
        });

        Optional<Integer> result = dialog.showAndWait();

        result.ifPresent(val -> {
            NodeDescriptor desc = new ConnectorDescriptor(val);
            insertToSelected(desc);
        });
    }
}
