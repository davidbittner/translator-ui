package unanet.translator;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.*;
import javafx.scene.Parent;
import javafx.scene.shape.*;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.ClipboardContent; 
import javafx.scene.input.Dragboard; 
import javafx.scene.input.TransferMode; 
import javafx.scene.input.DragEvent; 
import javafx.scene.effect.DropShadow; 
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Text;
import javafx.geometry.Pos;
import javafx.geometry.Point2D;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Font;
import javafx.beans.value.ChangeListener;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;

import java.util.UUID;
import unanet.translator.beans.*;

import java.lang.StringBuilder;
import java.io.*;

class TranslatorNode extends Pane {
    static final private double HEIGHT_PER_NODE = 30;
    static final private double LABEL_PADDING = 5;

    NodeEditor parent;

    Rectangle           block;
    Label               name;

    List<NodeConnecter> conns;
    List<Label>         conn_info;

    NodeDescriptor desc;

    double rel_grab_x, rel_grab_y;

    TranslatorNode(NodeEditor parent, NodeDescriptor desc, double pos_x, double pos_y) {
        this.parent = parent;
        this.desc = desc;

        this.parent.moveXProperty().addListener((obs, oldval, newval) -> {
            setLayoutX(getLayoutX() + newval.doubleValue());
        });

        this.parent.moveYProperty().addListener((obs, oldval, newval) -> {
            setLayoutY(getLayoutY() + newval.doubleValue());
        });

        conns = new ArrayList<>();

        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(5.0);
        dropShadow.setOffsetX(3.0);
        dropShadow.setOffsetY(3.0);
        dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

        setEffect(dropShadow);

        setHeight(
            HEIGHT_PER_NODE * 
            Math.max(desc.inputs.size(), 1)
        );

        name = new Label(desc.name);
        setWidth(desc.calculateWidth(LABEL_PADDING, name.getFont()) + (NodeConnecter.RADIUS*2) + NodeConnecter.RADIUS);

        double left_shift = 0.0;
        List<Label> names = new ArrayList<>();

        Text calc_text = new Text();
        for(int i = 0; i < desc.inputs.size(); i++) {
            double x = 0;
            double y = (getHeight()/desc.inputs.size()) * i;

            Label var_name = new Label(desc.inputs.get(i).name);
            var_name.setMinWidth(getWidth());
            var_name.setStyle(
                "-fx-font-size: 12;"
            );

            var_name.setMouseTransparent(true);
            var_name.setTextAlignment(TextAlignment.LEFT);
            var_name.setLayoutX(NodeConnecter.RADIUS + LABEL_PADDING);
            var_name.setLayoutY(y + (getHeight()/(desc.inputs.size()*2)) - NodeConnecter.RADIUS);

            NodeConnecter conn = new NodeConnecter(
                x,
                y + (getHeight()/(desc.inputs.size()*2)),
                desc.inputs.get(i),
                parent,
                this
            );

            var_name.applyCss();
            names.add(var_name);
            conns.add(conn);

            calc_text.setFont(var_name.getFont());
            calc_text.setText(var_name.getText());
            calc_text.prefWidth(-1);
            calc_text.setWrappingWidth(0);
            calc_text.setLineSpacing(0);

            left_shift = Math.max(calc_text.getLayoutBounds().getWidth(), left_shift);
        }
        
        if(desc.output != null) {
            double x = getWidth();
            double y = 0;

            Label var_name = new Label(desc.output.name);
            var_name.setMinWidth(getWidth());
            var_name.setStyle(
                "-fx-font-size: 12;"
            );

            var_name.setMouseTransparent(true);
            var_name.setAlignment(Pos.CENTER_RIGHT);
            var_name.setTextAlignment(TextAlignment.RIGHT);
            var_name.setLayoutX(-(NodeConnecter.RADIUS + LABEL_PADDING));
            var_name.setLayoutY(y + (getHeight()/2) - NodeConnecter.RADIUS);

            NodeConnecter conn = new NodeConnecter(
                x,
                y + (getHeight()/2),
                desc.output,
                parent,
                this
            );

            names.add(var_name);
            conns.add(conn);
        }

        name.setAlignment(Pos.CENTER_LEFT);
        name.setTextAlignment(TextAlignment.LEFT);
        name.setLayoutX(left_shift + (LABEL_PADDING * 2) + NodeConnecter.RADIUS);
        name.setMinWidth(getWidth());
        name.setMinHeight(getHeight());
        name.setStyle(
            "-fx-font-weight: bold;" +
            "-fx-font-size: 16;"
        );
        name.setMouseTransparent(true);

        block = new Rectangle(getWidth(), getHeight());
        block.setFill(Colors.FOREGROUND);
        block.setStroke(Colors.BACKGROUND);
        block.setArcHeight(10.0);
        block.setArcWidth(10.0);
        block.setStrokeWidth(3.0);

        Tooltip tip = new Tooltip(desc.desc);
        Tooltip.install(block, tip);

        getChildren().add(block);
        getChildren().add(name);
        getChildren().addAll(conns);
        getChildren().addAll(names);

        block.setOnMouseEntered(e -> {
            block.setFill(Colors.NODE_HOVER);
        });

        block.setOnMouseExited(e -> {
            block.setFill(Colors.FOREGROUND);
        });

        block.setOnMousePressed(e -> {
            rel_grab_x = e.getX();
            rel_grab_y = e.getY();

            block.setFill(Colors.NODE_DRAG);
            e.consume();
        });

        block.setOnMouseDragged(e -> {
            Point2D pos = new Point2D(e.getSceneX(), e.getSceneY());
            pos = parent.sceneToLocal(pos);

            setLayoutX(pos.getX() - rel_grab_x);
            setLayoutY(pos.getY() - rel_grab_y);
            e.consume();
        });

        block.setOnMouseReleased(e -> {
            block.setFill(Colors.NODE_HOVER);
            e.consume();

            parent.addState();
        });

        block.setOnMouseClicked(e -> {
            if(e.getButton() == MouseButton.SECONDARY) {
                parent.delete(this);
            }
        });

        setLayoutX(pos_x);
        setLayoutY(pos_y);
    }

    public String proc() {
        String ret = desc.code;

        for(NodeConnecter conn : conns) {
            if(conn.info.type == VarType.INPUT) {
                String conn_ret = conn.proc(); 
                if(conn_ret == null) {
                    ret = ret.replace("$"+conn.info.subst_name, "");
                }else{
                    ret = ret.replace("$"+conn.info.subst_name, conn_ret);
                }
            }
        }

        return ret;
    }

    public void registerConnecters() {
        for(NodeConnecter conn : conns) {
            parent.registerConnecter(conn);
        }
    }

    public void overrideIds(NodeState state) {
        for(ConnecterState conn_state : state.connectors) {
            for(NodeConnecter conn : conns) {
                if(conn_state.vari_key.equals(conn.info.subst_name)) {
                    conn.id = conn_state.id;
                }
            }
        }
    }
}
