package unanet.translator;

import javafx.scene.*;
import javafx.scene.shape.*;
import javafx.scene.paint.*;
import javafx.scene.canvas.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.input.DragEvent;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Text;
import javafx.scene.transform.Translate;
import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.geometry.Bounds;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.geometry.Pos;
import javafx.geometry.Point2D;
import javafx.util.Pair;
import javafx.application.Platform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import javafx.util.Pair;
import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;
import unanet.translator.beans.*;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.lang.StringBuilder;
import java.io.*;

public class NodeEditor extends Pane {
    static final int MAX_SAVED_STATES = 50;
    static final double SCALE_DELTA = 1.1;

    Map<String, NodeDescriptor> node_templates;

    HashMap<String, NodeConnecter> connectors;
    HashMap<String, String> connections;

    HashMap<String, ConnecterCurve> curves;

    ArrayList<EditorState> state_list;

    List<TranslatorNode> nodes;
    List<VisualCommentNode> comments;
    double total_x, total_y;

    TranslatorNode root;
    Label coords;

    double drag_x, drag_y;
    private DoubleProperty moveX, moveY;

    public DoubleProperty moveXProperty() {
        if(moveX == null) moveX = new SimpleDoubleProperty(0.0);

        return moveX;
    }

    public DoubleProperty moveYProperty() {
        if(moveY == null) moveY = new SimpleDoubleProperty(0.0);

        return moveY;
    }

    public void setMoveX(double x) {
        moveXProperty().set(x);
    }

    public void setMoveY(double y) {
        moveYProperty().set(y);
    }

    public NodeEditor(Map<String, NodeDescriptor> templates, boolean logic_editor) {
        node_templates = templates;

        nodes       = new ArrayList<>();
        comments    = new ArrayList<>();
        state_list  = new ArrayList<>();
        connectors  = new HashMap<>();
        connections = new HashMap<>();
        curves      = new HashMap<>();

        setOnScroll(e -> {
            e.consume();
            double scaleFactor = (e.getDeltaY() > 0)
                ? SCALE_DELTA
                : 1/SCALE_DELTA;

            setScaleX(getScaleX() * scaleFactor);
            setScaleY(getScaleY() * scaleFactor);
        });

        setOnMousePressed(e -> {
            drag_x = e.getX();
            drag_y = e.getY();

            e.consume();
        });

        setOnMouseDragged(e -> {
            setMoveX(e.getX() - drag_x);
            setMoveY(e.getY() - drag_y);

            total_x += (e.getX() - drag_x);
            total_y += (e.getY() - drag_y);
            coords.setText(String.format("(%.0f, %.0f)", total_x, total_y));

            drag_x = e.getX();
            drag_y = e.getY();

            e.consume();
        });

        setOnDragOver(e -> {
            Dragboard db = e.getDragboard();
            if(db.hasString()) {
                String node_id = db.getString();
                NodeDescriptor descrip = node_templates.get(node_id);

                if(descrip != null) {
                    e.acceptTransferModes(TransferMode.MOVE);
                }
            }

            e.consume();
        });

        setOnDragDropped(e -> {
            Dragboard db = e.getDragboard();
            String node_id = db.getString();

            if(!node_templates.containsKey(node_id)) {
                return;
            }
            addState();
            NodeDescriptor desc = node_templates.get(node_id);

            TranslatorNode add = new TranslatorNode(this, desc, e.getX(), e.getY());
            add.registerConnecters();

            nodes.add(add);
            getChildren().add(add);

            e.consume();
        });

        widthProperty().addListener(e -> {
            if(root == null && getWidth() != 0 && getHeight() != 0) {
                NodeDescriptor descript = null;
                if(logic_editor) {
                    descript = new LogicRootDescriptor();
                }else{
                    descript = new RootDescriptor();
                }

                root = new TranslatorNode(
                    this,
                    descript,
                    getWidth()/2.0,
                    getHeight()/2.0
                );
                root.registerConnecters();
                nodes.add(root);
                getChildren().add(root);
            }

            coords.setMinWidth(getWidth());
        });

        heightProperty().addListener(e -> {
            if(root == null && getWidth() != 0 && getHeight() != 0) {
                NodeDescriptor descript = null;
                if(logic_editor) {
                    descript = new LogicRootDescriptor();
                }else{
                    descript = new RootDescriptor();
                }

                root = new TranslatorNode(
                    this,
                    descript,
                    (getWidth()/2.0),
                    getHeight()/2.0
                );
                root.registerConnecters();
                nodes.add(root);
                getChildren().add(root);
            }

            coords.setMinHeight(getHeight());
        });

        coords = new Label();
        coords.setText(String.format("(%.0f, %.0f)", drag_x, drag_y));
        coords.setTextAlignment(TextAlignment.RIGHT);
        coords.setAlignment(Pos.BOTTOM_RIGHT);

        getChildren().add(coords);
    }

    public void createNewNode(NodeDescriptor desc) {
        addState();

        double x = getWidth()/2.0;
        double y = getHeight()/2.0;

        TranslatorNode add = new TranslatorNode(this, desc, x, y);
        add.registerConnecters();

        nodes.add(add);
        getChildren().add(add);
    }

    public void createNewComment(String text) {
        addState();

        double x = getWidth()/2.0;
        double y = getHeight()/2.0;

        VisualCommentNode node = new VisualCommentNode(this, text, x, y);
        comments.add(node);
        getChildren().add(node);
    }

    public void registerConnecter(NodeConnecter conn) {
        connectors.put(conn.id, conn);
    }

    public NodeConnecter connectedTo(String id) {
        String conn = connections.get(id);
        if(conn == null) {
            return null;
        }else{
            return getConnecter(conn);
        }
    }

    public NodeConnecter getConnecter(String id) {
        return connectors.get(id);
    }

    public void delete(TranslatorNode node) {
        if(node == root) {
            return;
        }

        for(NodeConnecter conn : node.conns) {
            disconnect(conn.id);
        }

        getChildren().remove(node);
        for(int i = 0; i < nodes.size(); i++) {
            if(nodes.get(i) == node) {
                nodes.remove(i);
                return;
            }
        }
    }

    public void deleteComment(VisualCommentNode node) {
        getChildren().remove(node);
        this.comments.remove(node);
    }

    public void newConnection(String id_a, String id_b) {
        disconnect(id_a);
        disconnect(id_b);

        connections.put(id_a, id_b);
        connections.put(id_b, id_a);

        ConnecterCurve curve = new ConnecterCurve(
            connectors.get(id_a),
            connectors.get(id_b)
        );

        curves.put(id_a, curve);
        curves.put(id_b, curve);

        getChildren().add(curve);
    }

    public void disconnect(String id) {
        String other_id = connections.get(id);

        connections.remove(id);
        connections.remove(other_id);

        curves.remove(id);
        ConnecterCurve curve = curves.remove(other_id);

        getChildren().remove(curve);
    }

    public String evaluate() {
        String ret = root.proc();
        return ret;
    }

    public void populate(EditorState state) {
        for(NodeState node : state.nodes) {
            if(node.desc instanceof RootDescriptor) {
                root = new TranslatorNode(this, node.desc, node.x, node.y);

                root.overrideIds(node);
                root.registerConnecters();
                nodes.add(root);

                getChildren().add(root);
            }else{
                TranslatorNode new_node = new TranslatorNode(this, node.desc, node.x, node.y);
                new_node.overrideIds(node);
                new_node.registerConnecters();

                nodes.add(new_node);
                getChildren().add(new_node);
            }
        }

        if(state.comments != null) {
            for(CommentState com : state.comments) {
                VisualCommentNode node = new VisualCommentNode(this, com.comment_text, com.x, com.y);
                this.comments.add(node);
                getChildren().add(node);
            }
        }

        for(Pair<String, String> conn : state.connections) {
            newConnection(conn.getKey(), conn.getValue());
        }
    }

    void addState() {
        EditorState current = new EditorState(this, null);
        state_list.add(current);

        //remove oldest
        if(state_list.size() > MAX_SAVED_STATES) {
            state_list.remove(0); 
        }
    }

    private void loadState(EditorState state) {
        getChildren().removeAll(nodes);
        getChildren().removeAll(curves.values());

        connections.clear();
        connectors.clear();
        curves.clear();
        nodes.clear(); 

        populate(state);
    }

    public void undo() {
        if(state_list.size() > 0) {
            loadState(state_list.get(state_list.size()-1));
            state_list.remove(state_list.size()-1);
        }
    }
}
