package unanet.translator.beans;

import org.simpleframework.xml.Root;

@Root(name="variable")
public class NodeInput extends NodeVariable {
    public NodeInput() {
        type = VarType.INPUT;
    }
}
