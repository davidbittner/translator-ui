package unanet.translator.beans;

import java.util.ArrayList;

public class RootDescriptor extends NodeDescriptor {
    static final long serialVersionUID = -73456436456299L;

    public RootDescriptor()
    {
        NodeInput input = new NodeInput();
        input.name = "Final";
        input.desc = "What to output for this column.";
        input.subst_name = "SUB";
        input.dat_type = DataType.STRING;

        inputs = new ArrayList<NodeInput>();
        inputs.add(input);
        output = null;

        name = "ROOT";
        desc = "The root of the entire column. What is inputted here, is what is outputted for this column.";

        code = "$SUB";
    }
}
