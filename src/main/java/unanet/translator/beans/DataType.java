package unanet.translator.beans;

public enum DataType {
    STRING,
    INT,
    BOOLEAN,
    EXPRESSION
}
