package unanet.translator.beans;

import java.util.ArrayList;
import java.lang.StringBuilder;

public class IfDescriptor extends NodeDescriptor {
    static final long serialVersionUID = -734564364123499L;

    public IfDescriptor(int elseifs, boolean haselse) {
        name = "IF-STATEMENT";
        desc = "An if-statement that resolves to a given branch depending on the supplied conditions.";

        StringBuilder code_block = new StringBuilder();
        ArrayList<NodeInput> inputs = new ArrayList<>();

        for(int i = 0; i < elseifs+1; i++) {
            NodeInput cond = new NodeInput();
            cond.desc = "A condition to be evaluated.";
            NodeInput expr = new NodeInput();
            expr.desc = "An expression to be evaluated based on the previous condition.";

            cond.dat_type = DataType.BOOLEAN;
            expr.dat_type = DataType.EXPRESSION;

            if(i == 0) {
                cond.name = "If Condition ";
                expr.name = "If Expression";

                cond.subst_name = String.format("COND_%d", i);
                expr.subst_name = String.format("EXPR_%d", i);

                code_block.append(String.format("IF($COND_%d) {\n", i));
                code_block.append(String.format("\t$EXPR_%d \n", i));
                code_block.append("}\n");
            }else{
                cond.name = "Else-If Condition ";
                expr.name = "Else-If Expression";

                cond.subst_name = String.format("COND_%d", i);
                expr.subst_name = String.format("EXPR_%d", i);

                code_block.append(String.format("ELSEIF($COND_%d) {\n", i));
                code_block.append(String.format("\t$EXPR_%d \n", i));
                code_block.append("}\n");
            }

            inputs.add(cond);
            inputs.add(expr);
        }

        if(haselse) {
            NodeInput elseexpr = new NodeInput();
            elseexpr.dat_type = DataType.EXPRESSION;
            elseexpr.name = "Else Expression";
            elseexpr.desc = "What to be evaluated if none of the above conditions are true.";
            elseexpr.subst_name = "ELSE";

            code_block.append("ELSE {\n");
            code_block.append("\t$ELSE\n");
            code_block.append("}\n");
            
            inputs.add(elseexpr);
        }

        NodeOutput output = new NodeOutput();
        output.name = "Out";
        output.desc = "The generated if-statement.";
        output.dat_type = DataType.EXPRESSION;

        this.inputs = inputs;
        this.output = output;
        this.group = null;
        this.code = code_block.toString();
    }
}
