package unanet.translator.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.lang.StringBuilder;
import java.io.*;

public class LoadFile implements Serializable {
    public transient StringProperty fileId;
    public transient StringProperty filePath;

    public StringProperty fileIdProperty() {
        if(fileId == null) fileId = new SimpleStringProperty(this, "");

        return fileId;
    }

    public void setFileId(String id) {
        fileIdProperty().set(id);
    }

    public void setFilePath(String path) {
        filePathProperty().set(path);
    }

    public String getFileId() {
        return fileIdProperty().get();
    }

    public String getFilePath() {
        return filePathProperty().get();
    }

    public StringProperty filePathProperty() {
        if(filePath == null) filePath = new SimpleStringProperty(this, ""); 

        return filePath;
    }

    public boolean use_number;
    public boolean is_relative;
    public long    line_no;

    @Override
    public String toString() {
        StringBuilder ret_str = new StringBuilder();
        if(!use_number) {
            ret_str.append("LOAD(");
            ret_str.append(String.format("\"%s\", \"%s\"", getFileId(), getFilePath()));
        }else{
            if(is_relative) {
                ret_str.append("LOADRELATIVEMIN(");
            }else{
                ret_str.append("LOADMIN(");
            }
            ret_str.append(String.format("\"%s\", \"%s\", \"%s\"", getFileId(), getFilePath(), line_no));
        }
        ret_str.append(")\n");

        return ret_str.toString();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();

        out.writeBoolean(use_number);
        out.writeBoolean(is_relative);
        out.writeLong(line_no);

        out.writeUTF(filePath.getValueSafe());
        out.writeUTF(fileId.getValueSafe());
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();

        use_number = in.readBoolean();
        is_relative = in.readBoolean();
        line_no = in.readLong();

        setFilePath(in.readUTF());
        setFileId(in.readUTF());
    }
}
