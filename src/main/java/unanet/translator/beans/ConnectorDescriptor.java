package unanet.translator.beans;

import java.util.ArrayList;
import java.lang.StringBuilder;

public class ConnectorDescriptor extends NodeDescriptor {
    static final long serialVersionUID = -73647567321L;

    public ConnectorDescriptor(int num) {
        name = "BLOCK CONNECTOR";
        
        NodeOutput output = new NodeOutput();
        output.name = "Combined";
        output.desc = "All of the inputs combined into one expression.";

        ArrayList<NodeInput> inputs = new ArrayList<>();
        StringBuilder code_block = new StringBuilder();
        for(int i = 0; i < num; i++) {
            NodeInput input = new NodeInput();
            input.name = String.format("%d", i+1); 
            input.desc = "An expression to be combined with other expressions.";
            input.subst_name = Integer.toString(i);

            code_block.append(String.format("$%d\n", i));
            
            inputs.add(input);
        }

        this.inputs = inputs;
        this.output = output;
        this.code = code_block.toString();
    }
}
