package unanet.translator.beans;

import java.util.ArrayList;

public class LogicRootDescriptor extends RootDescriptor {
    public LogicRootDescriptor() {
        NodeInput input = new NodeInput();
        input.name = "Logic";
        input.desc = "The final point of this column.";
        input.subst_name = "SUB";
        input.dat_type = DataType.STRING;

        inputs = new ArrayList<NodeInput>();
        inputs.add(input);
        output = null;

        name = "LOGIC ROOT";
        desc = "The root of all of the given logic.";

        code = "LOGICONLY()\n$SUB";
    }
}
