package unanet.translator.beans;

import java.util.ArrayList;
import java.lang.StringBuilder;

public class CommentDescriptor extends NodeDescriptor {
    static final long serialVersionUID = -1543651315442L;

    public CommentDescriptor(String commentText) {
        NodeInput input = new NodeInput();
        input.name = "Code";
        input.desc = "The code to put after the comment.";
        input.subst_name = "$CODE";
        input.dat_type = DataType.STRING;

        inputs = new ArrayList<NodeInput>();
        inputs.add(input);

        NodeOutput output = new NodeOutput();
        output.name = "Code";
        output.desc = "The code that now includes the comment.";
        this.output = output;

        name = "COMMENT";
        desc = commentText;

        StringBuilder code_builder = new StringBuilder();
        for(String line : commentText.split("\n")) {
            code_builder.append("#");
            code_builder.append(line);
            code_builder.append("\n");
        }
        code_builder.append("$CODE");

        code = code_builder.toString();
    }
}
