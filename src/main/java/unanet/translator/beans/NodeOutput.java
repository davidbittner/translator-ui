package unanet.translator.beans;

import org.simpleframework.xml.Root;

@Root(name="variable")
public class NodeOutput extends NodeVariable {
    public NodeOutput() {
        type = VarType.OUTPUT;
    }
}
