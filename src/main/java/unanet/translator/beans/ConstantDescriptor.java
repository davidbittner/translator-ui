package unanet.translator.beans;

import java.util.ArrayList;

public class ConstantDescriptor extends NodeDescriptor {
    static final long serialVersionUID = 5464562346254699L;

    public ConstantDescriptor(String value) {
        NodeOutput output = new NodeOutput();
        output.desc = "The constant value.";
        output.name = '"' + value + '"';
        this.output = output;

        inputs = new ArrayList<>();

        name = "Constant";
        desc = "A constant value node that simply outputs the value it is given.";
        code = '"' + value + '"';
    }
}
