package unanet.translator.beans;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.io.Serializable;

@Root(name="variable")
public class NodeVariable implements Serializable {
    @Attribute(name="name")
    public String name;

    @Element(name="description")
    public String desc;

    @Element(name="key", required=false)
    public String subst_name = "";

    @Element(name="type", required=true)
    public DataType dat_type;

    public VarType type;
}
