package unanet.translator.beans;

import java.util.List;
import java.util.ArrayList;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import java.io.Serializable;

import javafx.scene.text.Text;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Font;

import java.util.Arrays;

@Root(name="template")
public class NodeDescriptor implements Serializable {
    static final long serialVersionUID = -7588980448693010398L;

    @ElementList(name="inputs")
    public List<NodeInput> inputs;
    @Element(name="output")
    public NodeOutput output;

    @Attribute(name = "name")
    public String name;

    @Attribute(name="group")
    public String group;

    @Element(name = "description")
    public String desc;

    @Element(name = "code")
    public String code;

    public double calculateWidth(double padding, Font font) {
        double total = padding;

        Font name_font = font.font("System", FontWeight.BOLD, 16.0);
        Text other_text = new Text();

        if(inputs.size() > 0) {
            String longest_in = "";

            for(NodeInput in : inputs) {
                if(longest_in.length() < in.name.length()) {
                    longest_in = in.name;
                }
            }

            other_text.setFont(font.font(12.0));
            other_text.setText(longest_in);
            other_text.prefWidth(-1);
            other_text.setWrappingWidth(0);
            other_text.setLineSpacing(0);

            total += other_text.getLayoutBounds().getWidth();
            total += padding;
        }

        if(!name.equals("")) {
            Text name_text = new Text();
            name_text.setText(name);
            name_text.setFont(name_font);
            name_text.prefWidth(-1);
            name_text.setWrappingWidth(0);
            name_text.setLineSpacing(0);

            total += name_text.getLayoutBounds().getWidth();
            total += padding;
        }

        if(output != null) {
            other_text.setText(output.name);
            total += other_text.getLayoutBounds().getWidth();
            total += padding;
        }

        return total;
    }
}
