package unanet.translator.beans;

import org.simpleframework.xml.*;
import java.util.List;
import java.util.Map;

@Root(name="groups")
public class DescriptorGroupList {
    @ElementMap(attribute=true, inline=true, key="name", entry="group")
    public Map<String, NodeDescriptorGroup> nodes;
}
