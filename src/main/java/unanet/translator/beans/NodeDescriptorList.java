package unanet.translator.beans;

import java.util.List;
import java.util.ArrayList;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

@Root(name="templates")
public class NodeDescriptorList {
    @ElementList(inline=true)    
    public List<NodeDescriptor> nodes;
}
