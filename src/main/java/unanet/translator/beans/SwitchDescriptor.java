package unanet.translator.beans;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;

public class SwitchDescriptor extends NodeDescriptor {
    static final long serialVersionUID = -73647567326431L;

    public SwitchDescriptor(ObservableList<String> items, boolean def) {
        List<String> list = items.stream().collect(Collectors.toList());

        name = "SWITCH-STATEMENT";
        desc = "A switch statement that resolves to case matches the input string.";

        ArrayList<NodeInput> inputs = new ArrayList<>();
        NodeInput comp = new NodeInput();
        comp.name = "String";
        comp.desc = "The string used to try and find a case statement that matches this.";
        comp.subst_name = "COMP";

        inputs.add(comp);

        StringBuilder code_block = new StringBuilder();
        code_block.append("SWITCH($COMP) {\n");
        for(int i = 0; i < list.size(); i++) {
            NodeInput input = new NodeInput();
            input.name = '"' + list.get(i) + '"';
            input.desc = "A case for the switch to compare against.";
            input.subst_name = Integer.toString(i);

            code_block.append(String.format("\t CASE \"%s\" {\n", list.get(i)));
            code_block.append(String.format("\t$%d\n", i));
            code_block.append("\t}\n");

            inputs.add(input);
        }

        if(def) {
            NodeInput input = new NodeInput();
            input.name = "Default";
            input.desc = "The default condition. I.E. what's run if the given string doesn't match any of the cases.";
            input.subst_name = "DEFAULT";

            code_block.append("\tCASE DEFAULT {\n");
            code_block.append("\t$DEFAULT\n");
            code_block.append("}\n");
            
            inputs.add(input);
        }

        code_block.append("}\n");

        NodeOutput output = new NodeOutput();
        output.name = "Out";
        output.desc = "The generated switch statement.";
        output.dat_type = DataType.EXPRESSION;
        
        this.code = code_block.toString();
        this.inputs = inputs;
        this.output = output;
    }
}
