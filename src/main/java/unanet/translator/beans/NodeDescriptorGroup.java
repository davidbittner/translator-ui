package unanet.translator.beans;

import org.simpleframework.xml.*;
import java.util.List;
import java.util.Map;

@Root(name="group")
public class NodeDescriptorGroup {
    @ElementMap(attribute=true, inline=true, key="name", entry="template")
    public Map<String, NodeDescriptor> nodes;
}
