package unanet.translator;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.*;
import javafx.scene.Parent;
import javafx.scene.shape.*;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.ClipboardContent; 
import javafx.scene.input.Dragboard; 
import javafx.scene.input.TransferMode; 
import javafx.scene.input.DragEvent; 
import javafx.scene.effect.DropShadow; 
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;
import javafx.geometry.Pos;
import javafx.geometry.Point2D;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;

import unanet.translator.beans.*;

public class NodeDescriptorBox extends VBox {
    NodeDescriptor descriptor;

    Label name;
    Label desc;

    NodeDescriptorBox(NodeDescriptor descriptor) {
        setPadding(new Insets(5, 5, 5, 5));

        name = new Label(descriptor.name);
        desc = new Label(descriptor.desc);

        name.setStyle(
            "-fx-font-weight: bold;" +
            "-fx-font-size: 16;"
        );

        desc.setWrapText(true);

        getChildren().addAll(name, desc);

        setOnDragDetected(e -> {
            Dragboard db = startDragAndDrop(TransferMode.MOVE);
            ClipboardContent cont = new ClipboardContent();
            cont.putString(descriptor.group + descriptor.name);
            db.setContent(cont);

            e.consume();
        });
    }
}
