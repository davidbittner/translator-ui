package unanet.translator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Menu;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.application.Platform;
import javafx.stage.Stage;
import java.io.*;
import java.util.Arrays;
import java.util.Optional;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.core.Persister;

public class Main extends Application {
    public static void main(String []args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main_window.fxml"));
            Parent root = loader.load();
            MainWindowController cont = loader.getController();

            Scene scene = new WindowContainer(root);
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setTitle("Translator");

            scene.getWindow().setOnCloseRequest(e -> {
                if(cont.pendingChanges()) {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Unsaved Changes");
                    alert.setHeaderText("Unsaved Changes");
                    alert.setContentText("Are you sure you would like to continue without saving?");

                    Optional<ButtonType> button = alert.showAndWait();
                    if(button.get() == ButtonType.OK) {
                        Platform.exit();
                    }
                }else{
                    Platform.exit();
                }

                e.consume();
            });
        }catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
