package unanet.translator;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.*;
import javafx.scene.canvas.*;
import javafx.scene.Parent;
import javafx.scene.shape.*;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.input.MouseEvent; 
import javafx.scene.input.ClipboardContent; 
import javafx.scene.input.Dragboard; 
import javafx.scene.input.TransferMode; 
import javafx.scene.input.DragEvent; 
import javafx.scene.transform.Translate;
import javafx.scene.effect.DropShadow; 
import javafx.scene.control.Tooltip;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import unanet.translator.beans.*;
import unanet.translator.beans.DataType.*;

class NodeConnecter extends Circle {
    NodeEditor editor;
    TranslatorNode parent;

    static final double RADIUS = 8.0;
    String id;
    NodeVariable info;

    NodeConnecter(double x, double y, NodeVariable info, NodeEditor editor, TranslatorNode parent) {
        super(RADIUS);

        this.info = info;
        this.parent = parent;
        this.editor = editor;

        id = UUID.randomUUID().toString();
        editor.registerConnecter(this);

        /*
        switch(info.dat_type) {
            case STRING:
                setFill(Colors.STRING_TYPE);
                break;

            case INT:
                setFill(Colors.INT_TYPE);
                break;

            case EXPRESSION:
                setFill(Colors.EXPRESSION_TYPE);
                break;

            case BOOLEAN:
                setFill(Colors.BOOLEAN_TYPE);
                break;
        }
        */
        setFill(Colors.EXPRESSION_TYPE);

        setStrokeWidth(3.0);
        setStroke(Colors.ACCENT_B);
        setLayoutX(x);
        setLayoutY(y);

        Tooltip tip = new Tooltip(
            String.format(
                "%s: %s",
                (info.type == VarType.INPUT)?("Input"):("Output"),
                info.desc
            )
        );
        Tooltip.install(this, tip);

        setOnMouseEntered(e -> {
            setFill(Colors.ACCENT_B);
            setStroke(Colors.ACCENT_C);
        });

        setOnMouseExited(e -> {
            setFill(Colors.ACCENT_C);
            setStroke(Colors.ACCENT_B);
        });

        setOnDragDetected(e -> {
            Dragboard db = startDragAndDrop(TransferMode.MOVE);
            
            ClipboardContent cont = new ClipboardContent();
            cont.putString(this.id);
            db.setContent(cont);

            e.consume();
        });

        setOnDragOver(e -> {
            Dragboard db = e.getDragboard();
            if (db.hasString() && e.getGestureSource() != this) {
                String their_id = db.getString();

                //This is for excluding other drops, if it isn't a valid
                //ID you can assume that it was a drag and drop for something
                //else, such as adding a new node.
                NodeConnecter conn = editor.getConnecter(their_id);
                if(conn == null) {
                    return;
                }

                if(conn.info.type != info.type) {
                    e.acceptTransferModes(TransferMode.MOVE);
                }
            }

            setFill(Colors.ACCENT_B);
            setStroke(Colors.ACCENT_C);
            e.consume();
        });

        setOnDragDropped(e -> {
            Dragboard db = e.getDragboard();
            String their_id = db.getString();

            //This is for excluding other drops, if it isn't a valid
            //ID you can assume that it was a drag and drop for something
            //else, such as adding a new node.
            NodeConnecter conn = editor.getConnecter(their_id);
            if(conn == null) {
                return;
            }
            editor.addState();

            editor.newConnection(id, their_id);

            e.setDropCompleted(true);
        });

        setOnMouseDragged(e -> {
            e.consume();
        });

        setOnDragDone(e -> {
            if(e.getTransferMode() == null) {
                editor.disconnect(id);
            }

            e.consume();
        });
    }

    public String proc() {
        NodeConnecter conn = editor.connectedTo(id);
        if(conn == null) {
            return null;
        }
        return conn.parent.proc();
    }
}

