package unanet.translator;

import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.paint.*;
import javafx.scene.Parent;
import javafx.scene.shape.*;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ClipboardContent; 
import javafx.scene.input.Dragboard; 
import javafx.scene.input.TransferMode; 
import javafx.scene.control.Alert;
import javafx.scene.Node;
import javafx.scene.layout.Priority;
import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.DragEvent; 
import javafx.scene.effect.DropShadow; 
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.Text;
import javafx.geometry.Pos;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.geometry.Point2D;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Font;
import javafx.beans.value.ChangeListener;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import java.util.UUID;
import unanet.translator.beans.*;

import java.lang.StringBuilder;
import java.io.*;

class VisualCommentNode extends Pane {
    static final int FONT_SIZE = 12;
    static final int PADDING   = 5;

    NodeEditor parent;

    Label     text;
    Rectangle block;

    double rel_grab_x, rel_grab_y;

    VisualCommentNode(NodeEditor parent, String text, double x, double y) {
        this.parent = parent;
        this.text = new Label(text);
        this.text.setMouseTransparent(true);
        this.text.setLayoutX(PADDING);
        this.text.setLayoutY(PADDING);

        this.text.textProperty().addListener((obs, oldv, newv) -> {
            Text calc = new Text();
            calc.setFont(this.text.getFont());
            calc.setText(this.text.getText());
            calc.prefWidth(-1);
            calc.setWrappingWidth(0);
            calc.setLineSpacing(0);

            this.setWidth(calc.getLayoutBounds().getWidth());
            this.block.setWidth(calc.getLayoutBounds().getWidth() + PADDING * 2);

            long count = newv
                .chars()
                .filter(ch -> ch == '\n')
                .count() + 1;

            this.setHeight(calc.getLayoutBounds().getHeight() + (PADDING * 2));
            this.block.setHeight(calc.getLayoutBounds().getHeight() + (PADDING * 2));
        });

        long count = text
            .chars()
            .filter(ch -> ch == '\n')
            .count() + 1;

        Text calc_text = new Text();
        calc_text.setFont(this.text.getFont());
        calc_text.setText(this.text.getText());
        calc_text.prefWidth(-1);
        calc_text.setWrappingWidth(0);
        calc_text.setLineSpacing(0);

        setWidth(calc_text.getLayoutBounds().getWidth());
        setHeight(calc_text.getLayoutBounds().getHeight() + (PADDING * 2));

        setLayoutX(x);
        setLayoutY(y);

        block = new Rectangle(getWidth(), getHeight());
        block.setFill(Colors.COMMENT_BACKGROUND);
        block.setStroke(Colors.COMMENT_FOREGROUND);
        block.setArcHeight(2.0);
        block.setArcWidth(2.0);
        block.setStrokeWidth(1.0);
        block.setWidth(calc_text.getLayoutBounds().getWidth() + (PADDING * 2));
        this.block.setHeight(calc_text.getLayoutBounds().getHeight() + (PADDING * 2));
        this.parent.moveXProperty().addListener((obs, oldval, newval) -> {
            setLayoutX(getLayoutX() + newval.doubleValue());
        });

        this.parent.moveYProperty().addListener((obs, oldval, newval) -> {
            setLayoutY(getLayoutY() + newval.doubleValue());
        });

        setOnMousePressed(e -> {
            rel_grab_x = e.getX();
            rel_grab_y = e.getY();

            block.setFill(Colors.COMMENT_PBACKGROUND);
            e.consume();
        });
        
        setOnMouseDragged(e -> {
            Point2D pos = new Point2D(e.getSceneX(), e.getSceneY());
            pos = parent.sceneToLocal(pos);

            setLayoutX(pos.getX() - rel_grab_x);
            setLayoutY(pos.getY() - rel_grab_y);
            e.consume();
        });

        setOnMouseEntered(e -> {
            block.setFill(Colors.COMMENT_HBACKGROUND);
            e.consume();
        });

        setOnMouseExited(e -> {
            block.setFill(Colors.COMMENT_BACKGROUND);
        });

        setOnMouseReleased(e -> {
            block.setFill(Colors.COMMENT_HBACKGROUND);
            e.consume();

            parent.addState();
        });

        setOnMouseClicked(e -> {
            if(e.getButton().equals(MouseButton.PRIMARY)) {
                if(e.getClickCount() == 2) {
                    Optional<String> new_text = displayTextArea(this.text.getText());
                    new_text.ifPresent((t) -> {
                        this.text.setText(t);
                    });
                }
            }else if(e.getButton().equals(MouseButton.SECONDARY)) {
                parent.deleteComment(this);
            }
        });

        getChildren().add(this.block);
        getChildren().add(this.text);
    }

    static Optional<String> displayTextArea(String start_text) {
        Dialog<String> dialog = new Dialog<>();
        dialog.setWidth(400);
        dialog.setHeight(400);
        dialog.setTitle("Enter Comment Text");
        dialog.setHeaderText("Enter the new header text below:");

        ButtonType doneButton   = new ButtonType("Done", ButtonData.OK_DONE);
        ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

        dialog.getDialogPane().getButtonTypes().addAll(doneButton, cancelButton);
        HBox main_box = new HBox();
        TextArea text = new TextArea(start_text);
        main_box.setHgrow(text, Priority.ALWAYS);
        main_box.getChildren().add(text);

        Node doneButtonHandle = dialog.getDialogPane().lookupButton(doneButton);
        text.textProperty().addListener((ob, old, newv) -> {
            doneButtonHandle.setDisable(newv.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(main_box);
        Platform.runLater(() -> text.requestFocus());
        dialog.setResultConverter(button -> {
            if(button == doneButton) {
                return text.getText();
            }

            return null;
        });

        return dialog.showAndWait();
    }
}
