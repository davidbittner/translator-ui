package unanet.translator;

import java.util.ArrayList;
import java.util.List;
import javafx.util.Pair;
import java.io.Serializable;

import unanet.translator.beans.LoadFile;

public class HeaderBlock implements Serializable {
    public boolean unique, input_headers, sort;
    public String headers;
    public List<LoadFile> loads;

    public boolean has_logic_only;

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        ret.append(String.format("HEADER(\"%s\")\n", headers));
        ret.append(String.format("UNIQUE(\"%s\")\n", (unique)?("TRUE"):("FALSE")));
        ret.append(String.format("SORT(\"%s\")\n", (sort)?("TRUE"):("FALSE")));

        if(!input_headers) {
            ret.append("NOHEADERS()\n");
        }

        for(LoadFile load : loads) {
            ret.append(load.toString());
        }

        return ret.toString();
    }
}
