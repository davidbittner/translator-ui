package unanet.translator;

import javafx.scene.Scene;
import java.io.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import java.util.List;
import java.util.ArrayList;
import org.simpleframework.xml.core.Persister;
import unanet.translator.beans.*;

import java.util.HashMap;

public class WindowContainer extends Scene {
    final static int WINDOW_WIDTH = 800;
    final static int WINDOW_HEIGHT = 600;

    public WindowContainer(Parent root) throws Exception {
        super(root, WINDOW_WIDTH, WINDOW_HEIGHT);
    }
}
