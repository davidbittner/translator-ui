package unanet.translator;

import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.fxml.FXML;
import javafx.util.Pair;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.collections.transformation.SortedList;
import javafx.fxml.Initializable;
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.List;

import unanet.translator.beans.*;

public class NewTemplateController implements Initializable {
    @FXML
    public TextField headers;

    @FXML
    public Button new_lookup;
    @FXML
    public TableView<LoadFile> lookup_list;

    ObservableList<LoadFile> load_files;

    @FXML
    public TableColumn<LoadFile,String> id_column;
    @FXML
    public TableColumn<LoadFile,String> path_column;

    @FXML
    public CheckBox logic_only;
    @FXML
    public CheckBox unique;
    @FXML
    public CheckBox sort;
    @FXML
    public CheckBox inputs;

    HeaderBlock ret;

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        load_files = FXCollections.observableArrayList();
        lookup_list.setItems(load_files);

        id_column.setCellValueFactory(new PropertyValueFactory<LoadFile,String>("fileId"));
        path_column.setCellValueFactory(new PropertyValueFactory<LoadFile,String>("filePath"));
    }

    @FXML
    public void done() {
        ret = new HeaderBlock();
        try{
            ret.headers = headers.getText();
            ret.loads = load_files.stream().collect(Collectors.toList());

            ret.unique = unique.isSelected();
            ret.input_headers = inputs.isSelected();
            ret.sort = sort.isSelected();

            ret.has_logic_only = logic_only.isSelected();

            lookup_list.getScene().getWindow().hide();
        }catch(NullPointerException ex) {
            ret = null;

            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Fields Cannot be Empty");
            alert.setContentText("Some fields are invalid or left empty. Either close the header dialog, or fill them out to continue.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    @FXML
    public void spawnLookupDialog() {
        Stage dialog = new Stage(); 
        dialog.initOwner(new_lookup.getScene().getWindow());
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.setTitle("Lookup File");
        dialog.setResizable(true);
        
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("load_file_dialog.fxml"));
            Parent root = loader.load();
            LoadDialogController cont = (LoadDialogController)loader.getController();
            dialog.setScene(new Scene(root));

            dialog.showAndWait();
            LoadFile ret = cont.getReturn();
            
            if(ret != null) {
                load_files.add(ret);
            }
        }catch(IOException ex) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error Opening Dialog");
            alert.setContentText("Failed to open the dialog due to an IOException.");

            ex.printStackTrace();
            alert.showAndWait();
        }
    }

    HeaderBlock getBlock() {
        return ret;
    }
}
